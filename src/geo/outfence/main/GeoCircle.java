package geo.outfence.main;
public class GeoCircle {
	double latitude;
	double longitude;
	double radius ;
	
	public GeoCircle() {
		super();
	}

	public GeoCircle(double latitude, double longitude, double radius) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.radius = radius;
	}
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}

}

