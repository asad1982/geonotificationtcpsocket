package geo.outfence.main;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;

public class SocTcp {
	private int port  ;
	public SocTcp(int port) {
		this.port = port ;
	}
	public void init() throws IOException{
	    ServerSocket serverSocket = null;
	    Socket socket = null;
	    try {
			serverSocket = new ServerSocket(port);
	    }catch(IOException e) {
	    	e.printStackTrace();
	    }
	    System.out.println("Socket Started ......  ..");
	    while (true) {
		    try {
		         socket = serverSocket.accept();
		         System.out.println("Connection Accepted  ......  ..");
		         new GeofenceThread(socket).start();
		    }catch(IOException e) {
		    	e.printStackTrace();
		    }
	    }
	}
	public class GeofenceThread extends Thread {
		Geofence geofence = new Geofence();
		GeoCircle geoCircle = new GeoCircle();
	    protected Socket socket;
	    public GeofenceThread(Socket clientSocket) {
	        this.socket = clientSocket;
	    }
	    public void run() {
	        InputStream inp = null;
	        BufferedReader brinp = null;
	        DataOutputStream out = null;
	        try {
	            inp = socket.getInputStream();
	            brinp = new BufferedReader(new InputStreamReader(inp));
	            out = new DataOutputStream(socket.getOutputStream());
	        }catch (IOException eI) {
	            return;
	        }
	        String line;
	        while (true) {
	            try {
	                line = brinp.readLine();
	                if ((line == null) || line.equalsIgnoreCase("QUIT")) {
	                    socket.close();
	                    return;
	                } else {	                	
	                	String[] recData = line.split(",", 4); 	// r,center-lat,center-long,objCord-latitude,objCord-longitude.
	                	geoCircle.setRadius(Double.parseDouble(recData[0]));
	                	geoCircle.setLatitude(Double.parseDouble(recData[1]));
	                	geoCircle.setLongitude(Double.parseDouble(recData[2]));
	                	if(!geofence.checkInside(geoCircle, Double.parseDouble(recData[3]), Double.parseDouble(recData[4]))){
	                		//Object is out of geofence Area .
	                		 out.writeUTF("Object is out of geofence area ... ");
	                	}	                	
	                }
	            } catch (IOException e) {
	                e.printStackTrace();
	                return;
	            }
	        }
	    }
	}
	public static void main(String[] args) {
		SocTcp socketTcp = new SocTcp(9999);
		try {
			socketTcp.init();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}












